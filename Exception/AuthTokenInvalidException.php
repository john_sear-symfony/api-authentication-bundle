<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Exception;

class AuthTokenInvalidException extends \Exception
{

}
