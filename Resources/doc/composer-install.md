# Installation

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

## Composer Require

Open a command console, enter your project directory and execute:

```console
$ composer require jsp/api-authentication-bundle:"0.1.*"
```

---
Next Step, see [Symfony Configuration](symfony-configuration.md).
