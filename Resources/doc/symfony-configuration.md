# Symfony Configuration

## 1. Add config files
### Routing
Copy  ``__DIR__\Resources\config\routes\jsp_api_authentication.yaml.dist`` _without_ suffix ``.dist`` into ``\config\routes\`` Folder
 
### Packages
If you want to set configurations _by hand_, you can easily copy  ``__DIR__\Resources\config\packages\jsp_api_authentication.yaml.dist`` (_without_ suffix ``.dist``)  
into ``\config\packages\`` Folder and configure it. But this is not necessary ;-)

## 1. Security Configuration
To access an API and authenticate with a Token, just add following to security config

```yaml
security:
    # ...

    firewalls:

        # ...

        main:
            
            # ...

            guard:
                authenticators:
                    - JohnSear\JspAuthenticationBundle\Security\FormLoginAuthenticator
                    - JohnSear\JspApiAuthenticationBundle\Security\TokenAuthenticator
                entry_point: JohnSear\JspAuthenticationBundle\Security\FormLoginAuthenticator
```
