# Api Authentication

## JWT
By default a *J*SON *W*eb *T*oken can be used for every User.

### JWT Configuration by hand
If needed, in Configuration File simply add JWT setting for expiration time and encoding algorithm. But this is not necessary ;-) (see [Symfony Configuration](symfony-configuration.md)).

#### Expiration Time
Expiration Time __in seconds__, default is ``3600``.
> If ``-1`` is set or Symfony is in ``dev`` environment, there will be _no expiration_!

```yaml
jsp_api_authentication:
  
  # ...
  
  jwt:
    # ...
    expiration:
      time: 3600
```
#### Encoding Algorithm
Encoding Algorithm as string, default is ``sha256``. Other possible value is ``md5``

```yaml
jsp_api_authentication:

  # ...

  jwt:
    # ...
    algorithm:
      encoding: 'sha256' 
```

### Login
To generate or refresh JWT for a given user, use Users login / password credentials to login.

```bash
# basic usage
curl --location --request POST 'http://localhost:8000/jsp/api/v1/auth/login' --form 'login=%USER_LOGIN%' --form 'password=%USER_PSSWORD%'

# test with invalid credentials (wrong username)
# Response: {"message":"Username could not be found."}

# test with invalid credentials (wrong password)
# Response: {"message":"Invalid credentials."}

# test within valid credentials
# Response: {"jwt":"%JWT-VALUE%"}
```

### Request
Add ``Authorization: Bearer `` in Header with *JWT* as value to your api request, i.e. ```curl```.

```bash
# basic usage
curl --location --request POST 'http://localhost:8000/' --header 'Authorization: Bearer %JWT-VALUE%'

# test with a bad token (Bad Signature)
# Response: {"error_message":"Could not verify Token Signature."}

# test with a bad token (Expired Token)
# Response: {"error_message":"The Token has expired."}

# test with a working token
# the requested controller is executed: the page loads normally
```

## Auth Token
> This Authentication Method is ***deprecated*** and will be removed in further versions.  
> **Use JWT Authentication instead!**

### Add additional Api Token to User Configuration
In Configuration File simply add token and user uuid Information to given List (see [Symfony Configuration](symfony-configuration.md)).
> Token must be unique! 

```yaml
jsp_api_authentication:
  token_to_user:
    # System User
    - { token: 'a9ff1cfc74dd5774abb02aa6a3150e68e956e2a33fdb48308bea8763480d8ad9', user: 'c3afc3a8-08e2-8099-407e-46c2bbc2aa25' }
    
    # Additional Users
    - { token: 'NEW_TOKEN', user: 'USER_UUID' }
    - { token: 'NEW_TOKEN_2', user: 'USER_UUID_2' }
    # - { ...
```

### Request
Add ``X-AUTH-TOKEN`` as Header Key with API Key as value to your api request, i.e. ```curl```.

```bash
# test with a bad token
curl --location --request GET 'http://localhost:8000/' --header 'X-AUTH-TOKEN: %BAD%'
# {"message":"Username could not be found."}

# test with a working token
curl --location --request GET 'http://localhost:8000/' --header 'X-AUTH-TOKEN: %WORKING%'
# the requested controller is executed: the page loads normally
```
