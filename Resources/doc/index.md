# AccessControl Bundle Documentation

## Installation
* [With Composer](composer-install.md) (vendor Area)

## Configuration
* [Symfony Configuration](symfony-configuration.md)
* [API Authentication](api-authentication.md)
