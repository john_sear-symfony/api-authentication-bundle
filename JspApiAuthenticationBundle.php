<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle;

use JohnSear\JspApiAuthenticationBundle\DependencyInjection\JspApiAuthenticationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspApiAuthenticationBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspApiAuthenticationExtension();
    }
}
