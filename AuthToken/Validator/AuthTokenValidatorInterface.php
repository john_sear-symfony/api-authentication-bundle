<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Validator;

use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;

interface AuthTokenValidatorInterface
{
    public static function isValid($tokenValue):bool;

    /**
     * @throws AuthTokenInvalidException
     */
    public static function check(string $tokenString): string;
}
