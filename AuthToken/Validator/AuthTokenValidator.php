<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Validator;

use DateTime;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;

class AuthTokenValidator implements AuthTokenValidatorInterface
{
    public static function isValid($tokenValue): bool
    {
        try {
            self::check($tokenValue);
        } catch (AuthTokenInvalidException $e) {
            return false;
        }

        return true;
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public static function check(string $tokenValue): string
    {
        return self::validateStructure($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    private static function validateStructure(string $tokenValue): string
    {
        $parts = explode('.', $tokenValue);

        if (count($parts) !== 3) {
            throw new AuthTokenInvalidException('Wrong number of segments');
        }

        $parts = array_filter(array_map('trim', $parts));

        if (count($parts) !== 3 || implode('.', $parts) !== $tokenValue) {
            throw new AuthTokenInvalidException('Malformed token');
        }

        return $tokenValue;
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public static function isTokenIssuedAtExpired(DateTime $tokenIssuedAt, ?DateTime $tokenExpirationTime = null): bool
    {
        $nowTimestamp = (new DateTime())->getTimestamp();
        $iatTimestamp = $tokenIssuedAt->getTimestamp();

        if ($nowTimestamp < $iatTimestamp) {
            throw new AuthTokenInvalidException('The Token "issued at" parameter value is invalid.');
        }

        if ($tokenExpirationTime instanceof DateTime && $nowTimestamp > $tokenExpirationTime->getTimestamp()) {
            throw new AuthTokenInvalidException('The Token has expired.');
        }

        return false;
    }
}
