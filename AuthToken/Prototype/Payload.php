<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

use DateTime;

class Payload implements PayloadInterface
{
    /**
     * sub: Uuid from User
     * @var string
     */
    private $subject;

    /**
     * name: Login from User
     * @var string
     */
    private $name;

    /**
     * iat: issued at, timestamp of creation time
     * @var DateTime
     */
    private $issuedAt;

    /**
     * exp: expiration, timestamp the token will be expired
     * @var DateTime
     */
    private $expirationTime;

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): PayloadInterface
    {
        $this->subject = $subject;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): PayloadInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getIssuedAt(): DateTime
    {
        return $this->issuedAt;
    }

    public function setIssuedAt(DateTime $issuedAt): PayloadInterface
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    public function getExpirationTime(): ?DateTime
    {
        return $this->expirationTime;
    }

    public function setExpirationTime(DateTime $expirationTime): PayloadInterface
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    public function __toArray(): array
    {
        $toArray = [
            'sub' => $this->getSubject(),
            'name' => $this->getName(),
            'iat' => $this->getIssuedAt()->getTimestamp(),
        ];

        if ($this->getExpirationTime() instanceof DateTime) {
            $toArray['exp'] = $this->getExpirationTime()->getTimestamp();
        }

        return $toArray;
    }
}
