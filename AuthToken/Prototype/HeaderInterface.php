<?php

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

interface HeaderInterface
{
    public function getAlgorithm(): string;
    public function setAlgorithm(string $algorithm): HeaderInterface;

    public function getType(): string;
    public function setType(string $type): HeaderInterface;

    public function __toArray(): array;
}
