<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

interface TokenInterface
{
    public function get(): string;
}
