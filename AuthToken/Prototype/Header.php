<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

class Header implements HeaderInterface
{
    /** @var string */
    private $algorithm;
    /** @var string */
    private $type;

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function setAlgorithm(string $algorithm): HeaderInterface
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): HeaderInterface
    {
        $this->type = $type;

        return $this;
    }

    public function __toArray(): array
    {
        return [
            'alg' => $this->getAlgorithm(),
            'typ' => $this->getType()
        ];
    }
}
