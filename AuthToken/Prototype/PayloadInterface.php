<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

use DateTime;

interface PayloadInterface
{
    public function getSubject(): string;
    public function setSubject(string $subject): PayloadInterface;

    public function getName(): string;
    public function setName(string $name): PayloadInterface;

    public function getIssuedAt(): DateTime;
    public function setIssuedAt(DateTime $issuedAt): PayloadInterface;

    public function getExpirationTime(): ?DateTime;
    public function setExpirationTime(DateTime $expirationTime): PayloadInterface;

    public function __toArray(): array;
}
