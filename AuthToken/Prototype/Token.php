<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype;

use JohnSear\JspApiAuthenticationBundle\AuthToken\Validator\AuthTokenValidator;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;

class Token implements TokenInterface
{
    /** @var string */
    private $value;

    /**
     * @throws AuthTokenInvalidException
     */
    public function __construct(string $value)
    {
        $this->value = AuthTokenValidator::check($value);
    }

    public function get(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->get();
    }
}
