<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken;

class AuthTokenEncoder
{
    public static function base64url_encode($data): string
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public static function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
}
