<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\DataTransferObject;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenManager;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenProvider;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\HeaderInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\PayloadInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\Token;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\TokenInterface;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidTokenCreationArgumentException;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\JspUserBundle\Entity\User;

class TokenInformationDtoBuilder implements TokenInformationDtoBuilderInterface
{
    /** @var UserManager */
    private $userManager;
    /** @var AuthTokenManager */
    private $authTokenManager;
    /** @var AuthTokenProvider */
    private $authTokenProvider;

    public function __construct(UserManager $userManager, AuthTokenManager $authTokenManager, AuthTokenProvider $authTokenProvider)
    {
        $this->userManager = $userManager;
        $this->authTokenManager = $authTokenManager;
        $this->authTokenProvider = $authTokenProvider;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     * @throws InvalidTokenCreationArgumentException
     */
    public function buildDtoFromUserLogin(string $login): TokenInformationDtoInterface
    {
        if ($login === '' || !(($user = $this->userManager->getUserByLogin($login)) instanceof User)) {
            return new TokenInformationDto();
        }

        $token = $this->authTokenManager->encode($login);

        return $this->buildDto($user, $token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     */
    public function buildDtoFromTokenValue(string $tokenValue): TokenInformationDtoInterface
    {
        if ($tokenValue === '') {
            return new TokenInformationDto();
        }

        $token = $this->createTokenFromTokenValue($tokenValue);
        $user  = $this->getUserFromTokenValue($tokenValue);

        if (!$user instanceof User) {
            return new TokenInformationDto();
        }

        return $this->buildDto($user, $token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     */
    private function buildDto(User $user, TokenInterface $token): TokenInformationDtoInterface
    {
        $header            = $this->getHeaderFromToken($token);
        $payload           = $this->getPayloadFromToken($token);
        $verifiedSignature = $this->isSignatureVerified($token, $user);

        return (new TokenInformationDto())
            ->setUser($user)
            ->setToken($token)
            ->setHeader($header)
            ->setPayload($payload)
            ->setVerifiedSignature($verifiedSignature)
        ;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    private function getUserFromTokenValue(string $tokenValue): ?User
    {
        $token = $this->createTokenFromTokenValue($tokenValue);
        $payload = $this->getPayloadFromToken($token);

        $tokenUserUuid = $payload->getSubject();

        return $this->userManager->getUser($tokenUserUuid);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    private function createTokenFromTokenValue(string $tokenValue): TokenInterface
    {
        return new Token($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    private function getHeaderFromToken(TokenInterface $token): HeaderInterface
    {
        $encodedHeader = $this->authTokenProvider->getTokenFactory()->getEncodedHeaderFromToken($token);

        return $this->authTokenProvider->getHeaderFactory()->createHeaderFromEncodedHeader($encodedHeader);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    private function getPayloadFromToken(TokenInterface $token): PayloadInterface
    {
        $encodedPayload = $this->authTokenProvider->getTokenFactory()->getEncodedPayloadFromToken($token);

        return $this->authTokenProvider->getPayloadFactory()->createPayloadFromEncodedPayload($encodedPayload);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    private function isSignatureVerified(TokenInterface $token, User $user): bool
    {
        return $this->authTokenProvider->getTokenFactory()->verifySignatureFromTokenAndSalt($token, $user->getSalt());
    }
}
