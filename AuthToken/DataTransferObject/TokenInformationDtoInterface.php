<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\DataTransferObject;

use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\HeaderInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\PayloadInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\TokenInterface;
use JohnSear\JspUserBundle\Entity\User;

interface TokenInformationDtoInterface
{
    public function getUser(): ?User;
    public function setUser(User $user): TokenInformationDtoInterface;

    public function getToken(): ?TokenInterface;
    public function setToken(TokenInterface $token): TokenInformationDtoInterface;

    public function getHeader(): ?HeaderInterface;
    public function setHeader(HeaderInterface $header): TokenInformationDtoInterface;

    public function getPayload(): ?PayloadInterface;
    public function setPayload(PayloadInterface $payload): TokenInformationDtoInterface;

    public function isVerifiedSignature(): bool;
    public function setVerifiedSignature(bool $verifiedSignature): TokenInformationDtoInterface;
}
