<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\DataTransferObject;

use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidTokenCreationArgumentException;

interface TokenInformationDtoBuilderInterface
{
    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     * @throws InvalidTokenCreationArgumentException
     */
    public function buildDtoFromUserLogin(string $login): TokenInformationDtoInterface;

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     * @throws InvalidTokenCreationArgumentException
     */
    public function buildDtoFromTokenValue(string $tokenValue): TokenInformationDtoInterface;
}
