<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken;

use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\HeaderFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\PayloadFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\TokenFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\HeaderInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\PayloadInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\TokenInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Validator\AuthTokenValidator;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspUserBundle\Entity\User;

class AuthTokenProvider
{
    /** @var HeaderFactory  */
    private $headerFactory;
    /** @var PayloadFactory  */
    private $payloadFactory;
    /** @var TokenFactory  */
    private $tokenFactory;

    public function __construct(HeaderFactory $headerFactory, PayloadFactory $payloadFactory, TokenFactory $tokenFactory)
    {
        $this->headerFactory = $headerFactory;
        $this->payloadFactory = $payloadFactory;
        $this->tokenFactory = $tokenFactory;
    }

    public function getHeaderFactory(): HeaderFactory
    {
        return $this->headerFactory;
    }

    public function getPayloadFactory(): PayloadFactory
    {
        return $this->payloadFactory;
    }

    public function getTokenFactory(): TokenFactory
    {
        return $this->tokenFactory;
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function encode(User $user, HeaderInterface $header, PayloadInterface $payload, string $salt = null): TokenInterface
    {
        $encodedHeader = $this->getHeaderFactory()->createEncodedHeader($header);
        $encodedPayload = $this->getPayloadFactory()->createEncodedPayload($payload);

        $algorithmName = $this->getHeaderFactory()->getAlgorithmByAlgorithmName($header->getAlgorithm());
        $data          = $this->getTokenFactory()->createTokenValueData($encodedHeader, $encodedPayload);
        $secret        = (is_string($salt) && $salt !== '') ? $salt : $user->getSalt();

        $verifySignature = $this->getTokenFactory()->createVerifySignature($algorithmName, $data, $secret);

        return $this->getTokenFactory()->create($data, $verifySignature);
    }

    /**
     * @throws InvalidHeaderConvertException
     * @throws AuthTokenInvalidException
     */
    public function decodeHeaderFromToken(TokenInterface $token): HeaderInterface
    {
        $encodedHeader = $this->getTokenFactory()->getEncodedHeaderFromToken($token);

        return $this->getHeaderFactory()->createHeaderFromEncodedHeader($encodedHeader);
    }

    /**
     * @throws InvalidPayloadConvertException
     * @throws AuthTokenInvalidException
     */
    public function decodePayloadFromToken(TokenInterface $token): PayloadInterface
    {
        $encodedPayload = $this->getTokenFactory()->getEncodedPayloadFromToken($token);

        return $this->getPayloadFactory()->createPayloadFromEncodedPayload($encodedPayload);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function isTokenIssuedAtExpired(TokenInterface $token): bool
    {
        $tokenPayload = $this->decodePayloadFromToken($token);

        return AuthTokenValidator::isTokenIssuedAtExpired($tokenPayload->getIssuedAt(), $tokenPayload->getExpirationTime());
    }
}
