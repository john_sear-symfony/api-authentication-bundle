<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken;

use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\HeaderInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\PayloadInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\Token;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\TokenInterface;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidTokenCreationArgumentException;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use JohnSear\JspUserBundle\Entity\User;

class AuthTokenManager
{
    /** @var UserManager */
    private $userManager;
    /** @var AuthTokenProvider */
    private $provider;

    public function __construct(UserManager $userManager, AuthTokenProvider $provider)
    {
        $this->userManager = $userManager;
        $this->provider = $provider;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidTokenCreationArgumentException
     */
    public function encode(string $userLogin, string $salt = null): TokenInterface
    {
        $user = $this->userManager->getUserByLogin($userLogin);

        if (!$user instanceof User) {
            throw new InvalidTokenCreationArgumentException('No User found by Username');
        }

        $salt = (is_string($salt) && $salt !== '') ? $salt : $user->getSalt();

        $header =  $this->provider->getHeaderFactory()->create();
        $payload = $this->provider->getPayloadFactory()->create($user);

        return $this->provider->encode($user, $header, $payload, $salt);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    public function decodeHeader(string $tokenValue): HeaderInterface
    {
        $token = new Token($tokenValue);

        return $this->provider->decodeHeaderFromToken($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function decodePayload(string $tokenValue): PayloadInterface
    {
        $token = new Token($tokenValue);

        return $this->provider->decodePayloadFromToken($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function isTokenExpired(string $tokenValue): bool
    {
        $token = new Token($tokenValue);

        return $this->provider->isTokenIssuedAtExpired($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     * @throws InvalidHeaderConvertException
     */
    public function isTokenSignatureVerified(string $tokenValue): bool
    {
        $payload = $this->decodePayload($tokenValue);

        $userUuid = $payload->getSubject();

        $user = $this->userManager->getUser($userUuid);

        if (!$user instanceof User) {
            throw new AuthTokenInvalidException('Could not resolve User from Payload Data.');
        }

        $token = new Token($tokenValue);

        $isVerified = $this->provider->getTokenFactory()->verifySignatureFromTokenAndSalt($token, $user->getSalt());

        if (!$isVerified) {
            throw new AuthTokenInvalidException('Could not verify Token Signature.');
        }

        return $isVerified;
    }
}
