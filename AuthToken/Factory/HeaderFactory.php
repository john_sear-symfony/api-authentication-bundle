<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Factory;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenEncoder;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\Header;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\HeaderInterface;
use JohnSear\JspApiAuthenticationBundle\DependencyInjection\Configuration as CFG;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use function json_encode;

class HeaderFactory
{
    public const ALG_MD5      = 'md5';
    public const ALG_MD5_NAME = 'MD5';
    public const ALG_HS256      = 'sha256';
    public const ALG_HS256_NAME = 'HS256';

    private $algorithmNames = [
        self::ALG_MD5 => self::ALG_MD5_NAME,
        self::ALG_HS256 => self::ALG_HS256_NAME
    ];

    /** @var string */
    private $algorithm;

    public function __construct(string $algorithm = CFG::JWT_DEFAULT_ALGORITHM)
    {
        $this->algorithm = $algorithm;
    }

    public function create(): HeaderInterface
    {
        $algorithmName = $this->getAlgorithmNameByAlgorithm($this->algorithm);

        return (new Header())
            ->setAlgorithm($algorithmName)
            ->setType('JWT');
    }

    public function getAlgorithmNameByAlgorithm(string $algorithm): string
    {
        return array_key_exists($algorithm, $this->algorithmNames) ? $this->algorithmNames[$algorithm] : self::ALG_HS256_NAME;
    }

    public function getAlgorithmByAlgorithmName(string $algorithmName): string
    {
        $algorithms = array_keys($this->algorithmNames);

        return array_key_exists($algorithmName, $algorithms) ? $algorithmName : self::ALG_HS256;
    }

    public function convertHeaderToJson(HeaderInterface $header): string
    {
        return json_encode($header->__toArray());
    }

    /**
     * @throws InvalidHeaderConvertException
     */
    public function convertJsonToHeader($headerJson): HeaderInterface
    {
        $headerArray = json_decode($headerJson, true);

        if (!array_key_exists('alg', $headerArray) || !array_key_exists('typ', $headerArray)) {
            throw new InvalidHeaderConvertException('No valid Header Data given');
        }

        return (new Header())
            ->setAlgorithm($headerArray['alg'])
            ->setType($headerArray['typ']);
    }

    public function createEncodedHeader(HeaderInterface $header): string {
        $headerJson = $this->convertHeaderToJson($header);
        return AuthTokenEncoder::base64url_encode($headerJson);
    }

    /**
     * @throws InvalidHeaderConvertException
     */
    public function createHeaderFromEncodedHeader(string $encodedHeader): HeaderInterface
    {
        $headerJson = AuthTokenEncoder::base64url_decode($encodedHeader);

        return $this->convertJsonToHeader($headerJson);
    }
}
