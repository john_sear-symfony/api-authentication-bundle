<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Factory;

use DateInterval;
use DateTime;
use Exception;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenEncoder;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\Payload;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\PayloadInterface;
use JohnSear\JspApiAuthenticationBundle\DependencyInjection\Configuration as CFG;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspUserBundle\Entity\User;
use function json_encode;

class PayloadFactory
{
    /** @var int */
    private $expirationTime;

    public function __construct(int $expirationTime = CFG::JWT_DEFAULT_EXPIRATION_TIME)
    {
        $this->expirationTime = $expirationTime;
    }

    public function create(User $user): PayloadInterface
    {
        $payload = (new Payload())
            ->setSubject($user->getId())
            ->setName($user->getLogin())
            ->setIssuedAt(new DateTime());

        $iat = $payload->getIssuedAt();
        $exp = $this->calculateExpirationDate($iat);

        if ($exp->getTimestamp() !== $iat->getTimestamp()) {
            $payload->setExpirationTime($exp);
        }

        return $payload;
    }

    private function calculateExpirationDate(DateTime $iat, ?int $expirationTime = null): DateTime
    {
        $exp = $iat;
        $expirationTime = ($expirationTime !== null && is_int($expirationTime)) ? $expirationTime : $this->expirationTime;

        try {
            $di = new DateInterval('PT' . $expirationTime . 'S');
        } catch (Exception $ex) {
            $di = null;
        }

        if ($expirationTime !== -1 && $di instanceof DateInterval) {
            $exp = (new DateTime())->setTimestamp($iat->getTimestamp());
            $exp->add($di);
        }

        return $exp;
    }

    public function convertPayloadToJson(PayloadInterface $payload): string
    {
        return json_encode($payload->__toArray());
    }

    /**
     * @throws InvalidPayloadConvertException
     */
    public function convertJsonToPayload(string $payloadJson): PayloadInterface
    {
        $payloadArray = json_decode($payloadJson, true);

        if (
            !array_key_exists('sub', $payloadArray) ||
            !array_key_exists('name', $payloadArray) ||
            !array_key_exists('iat', $payloadArray)
        ) {
            throw new InvalidPayloadConvertException('No valid Payload Data given');
        }

        $payload = (new Payload())
            ->setSubject($payloadArray['sub'])
            ->setName($payloadArray['name'])
            ->setIssuedAt((new DateTime())->setTimestamp($payloadArray['iat']));

        if (array_key_exists('exp', $payloadArray)) {
            $payload->setExpirationTime((new DateTime())->setTimestamp($payloadArray['exp']));
        }

        return $payload;
    }

    public function createEncodedPayload(PayloadInterface $payload): string {
        $headerJson = $this->convertPayloadToJson($payload);

        return AuthTokenEncoder::base64url_encode($headerJson);
    }

    /**
     * @throws InvalidPayloadConvertException
     */
    public function createPayloadFromEncodedPayload(string $encodedPayload): PayloadInterface
    {
        $payloadJson = AuthTokenEncoder::base64url_decode($encodedPayload);

        return $this->convertJsonToPayload($payloadJson);
    }
}
