<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\AuthToken\Factory;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenEncoder;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\Token;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Prototype\TokenInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Validator\AuthTokenValidator;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;

class TokenFactory
{
    /** @var HeaderFactory */
    private $headerFactory;

    public function __construct(HeaderFactory $headerFactory)
    {
        $this->headerFactory = $headerFactory;
    }

    /**
     * @throws  AuthTokenInvalidException
     */
    public function create(string $data, string $verifySignature): TokenInterface
    {
        $tokenValue = $data . '.' . $verifySignature;

        return new Token($tokenValue);
    }

    public function createTokenValueData(string $encodedHeader, string $encodedPayload): string
    {
        return $encodedHeader . '.' . $encodedPayload;
    }

    public function createVerifySignature(string $algorithm, string $data, string $secret): string
    {
        return hash_hmac($algorithm, $data, AuthTokenEncoder::base64url_encode($secret));
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getTokenPartsFromToken(TokenInterface $token): array
    {
        $tokenValue = $token->get();

        return $this->getTokenPartsFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getTokenPartsFromTokenValue(string $tokenValue): array
    {
        $tokenValue = AuthTokenValidator::check($tokenValue);

        return preg_split('#\.#',$tokenValue, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedHeaderFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedHeaderFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedHeaderFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[0];
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedPayloadFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedPayloadFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedPayloadFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[1];
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedVerifySignatureFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedVerifySignatureFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedVerifySignatureFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[2];
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    public function verifySignatureFromTokenAndSalt(TokenInterface $token, string $salt): bool
    {
        $encodedHeader          = $this->getEncodedHeaderFromToken($token);
        $encodedPayload         = $this->getEncodedPayloadFromToken($token);
        $encodedVerifySignature = $this->getEncodedVerifySignatureFromToken($token);

        $headerFactory = $this->headerFactory;
        $header = $headerFactory->createHeaderFromEncodedHeader($encodedHeader);
        $algorithmName = $header->getAlgorithm();

        $algorithm = $headerFactory->getAlgorithmByAlgorithmName($algorithmName);

        $newData = $this->createTokenValueData($encodedHeader, $encodedPayload);
        $newVerifySignature = $this->createVerifySignature($algorithm, $newData, $salt);

        $newToken = $this->create($newData, $newVerifySignature);

        $newEncodedVerifySignature = $this->getEncodedVerifySignatureFromToken($newToken);

        return ($encodedVerifySignature === $newEncodedVerifySignature);
    }
}
