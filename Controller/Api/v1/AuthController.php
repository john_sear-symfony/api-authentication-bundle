<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Controller\Api\v1;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenManager;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidTokenCreationArgumentException;
use JohnSear\JspApiBundle\DependencyInjection\Controller\AbstractApiController as ApiController;
use JohnSear\JspUserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/auth", name="auth_")
 */
class AuthController extends ApiController
{
    /**
     * @Route("/login", name="login", methods={"GET", "POST"})
     * @IsGranted("IS_AUTHENTICATED_ANONYMOUSLY")
     *
     * @throws AuthTokenInvalidException
     * @throws InvalidTokenCreationArgumentException
     */
    public function login(): Response
    {
        if ($this->getUser() instanceof User && $this->getUser()->getUsername() !== 'system') {
            /** @var AuthTokenManager $authTokenManager */
            $authTokenManager = $this->get('jsp.api_authentication.auth_token.manager');

            $freshToken = $authTokenManager->encode($this->getUser()->getUsername());

            return $this->json(['jwt' => $freshToken->get()]);
        }

        return $this->json(['message' => 'Please login!']);
    }
}
