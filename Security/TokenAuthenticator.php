<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenManager;
use JohnSear\JspApiAuthenticationBundle\DependencyInjection\Configuration as CFG;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidHeaderConvertException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidPayloadConvertException;
use JohnSear\JspUserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var AuthTokenManager */
    private $authTokenManager;
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var array */
    private $apiKeys;

    public function __construct(EntityManagerInterface $em, AuthTokenManager $authTokenManager, UserPasswordEncoderInterface $passwordEncoder, array $apiKeys)
    {
        $this->em = $em;
        $this->authTokenManager = $authTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->apiKeys = $apiKeys;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        $hasBearer = (
            $request->headers->has('authorization') &&
            is_string($request->headers->get('authorization')) &&
            strpos($request->headers->get('authorization'), 'Bearer ') !== false
        );

        $hasAuthToken = $request->headers->has('X-AUTH-TOKEN');

        $isApiAuthLogin = (
            strpos($request->attributes->get('_route'), 'jsp_api_')    !== false &&
            strpos($request->attributes->get('_route'), '_auth_login') !== false &&
            $request->isMethod('POST')
        );

        return ($hasBearer || $hasAuthToken || $isApiAuthLogin);
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        $credentials = '';
        if (
            $request->headers->has('authorization') &&
            is_string($request->headers->get('authorization')) &&
            strpos($request->headers->get('authorization'), 'Bearer ') !== false
        ) {
            $credentials = $request->headers->get('authorization');
        }

        if ($credentials === '' && $request->headers->has('X-AUTH-TOKEN')) {
            $credentials = $request->headers->get('X-AUTH-TOKEN');
        }

        if ($credentials === '' && $request->request->has('login') && $request->request->has('password')) {
            $credentials = [
                'login' => $request->request->get('login'),
                'password' => $request->request->get('password'),
            ];
        }

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        $user = null;

        if (is_array($credentials) && array_key_exists('login', $credentials)) {
            $user = $this->em->getRepository(User::class)->findOneBy(['login' => $credentials['login']]);
        }

        if (!$user instanceof User && is_string($credentials) && strpos($credentials, 'Bearer ') !== false) {
            $userUuid = $this->findUserUuidByBearer($credentials);
            $user = $this->em->getRepository(User::class)->find($userUuid);
        }

        if (!$user instanceof User && is_string($credentials)) {
            $userUuid = $this->findUserUuidByToken($credentials);
            $user = $this->em->getRepository(User::class)->find($userUuid);
        }

        // if a User is returned, checkCredentials() is called
        return $user;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     * @throws InvalidHeaderConvertException
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $valid = false;

        if (is_array($credentials) && array_key_exists('password', $credentials)) {
            $valid = $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
        }

        if (!$valid && is_string($credentials) && strpos($credentials, 'Bearer ') === false) {
            // Check credentials - e.g. make sure the password is valid.
            // In case of an X-AUTH API token, no credential check is needed.

            // Return `true` to cause authentication success
            $valid = true;
        }

        if (!$valid && is_string($credentials) && strpos($credentials, 'Bearer ') !== false) {
            $tokenValue = str_replace('Bearer ', '', $credentials);

            $isTokenSignatureVerified = $this->authTokenManager->isTokenSignatureVerified($tokenValue);

            $isTokenExpired = $this->authTokenManager->isTokenExpired($tokenValue);

            $valid = ($isTokenSignatureVerified && !$isTokenExpired);
        }

        return $valid;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null): ?Response
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }

    private function findUserUuidByToken(string $token): string
    {
        return (
            array_key_exists($token, $this->apiKeys) &&
            array_key_exists(CFG::KEY_TTU_USER, $this->apiKeys[$token])
        )
            ? $this->apiKeys[$token][CFG::KEY_TTU_USER]
            : '';
    }

    private function findUserUuidByBearer(string $token): string
    {
        try {
            $tokenValue = str_replace('Bearer ', '', $token);
            $payload = $this->authTokenManager->decodePayload($tokenValue);
            $userUuid = $payload->getSubject();
        } catch (AuthTokenInvalidException | InvalidPayloadConvertException $ex) {
            $userUuid = '';
        }

        return $userUuid;
    }
}
