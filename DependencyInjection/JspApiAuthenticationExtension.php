<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\DependencyInjection;

use Exception;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenManager;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenProvider;
use JohnSear\JspApiAuthenticationBundle\AuthToken\DataTransferObject\TokenInformationDtoBuilder;
use JohnSear\JspApiAuthenticationBundle\AuthToken\DataTransferObject\TokenInformationDtoBuilderInterface;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\HeaderFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\PayloadFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\TokenFactory;
use JohnSear\JspApiAuthenticationBundle\Controller\Api\v1\AuthController;
use JohnSear\JspApiAuthenticationBundle\DependencyInjection\Configuration as CFG;
use JohnSear\JspApiAuthenticationBundle\Security\TokenAuthenticator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class JspApiAuthenticationExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;
    /** @var array */
    private $config;

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->container = $container;

        $this->config = $this->retrieveConfiguration($configs);

        $this->registerServices();
    }

    private function retrieveConfiguration(array $configs): array
    {
        $configuration = new Configuration();

        return $this->processConfiguration($configuration, $configs);
    }

    private function registerServices(): void
    {
        $this->registerHeaderFactory();
        $this->registerPayloadFactory();
        $this->registerTokenFactory();
        $this->registerAuthTokenProvider();
        $this->registerAuthTokenManager();
        $this->registerTokenAuthenticator();
        $this->registerTokenInformationDtoBuilder();
        $this->registerApiV1AuthController();
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerHeaderFactory(): void
    {
        $definition = new Definition(HeaderFactory::class, [
            $this->config[CFG::KEY_JWT][CFG::KEY_JWT_ENCODING][CFG::KEY_JWT_ALGORITHM]
        ]);

        $this->container->setDefinition('jsp.api_authentication.auth_token.header_factory', $definition)
            ->setPublic(true);
        $this->container->setAlias(HeaderFactory::class,'jsp.api_authentication.auth_token.header_factory')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerPayloadFactory(): void
    {
        $expirationTime = $this->config[CFG::KEY_JWT][CFG::KEY_JWT_EXPIRATION][CFG::KEY_JWT_EXPIRATION_TIME];

        try {
            $this->container->getParameter('kernel.environment');
            if ($this->container->getParameter('kernel.environment') === 'dev') {
                $expirationTime = -1;
            }
        } catch (Exception $ex) {
          // FlowControlException
        }

        $definition = new Definition(PayloadFactory::class, [
            $expirationTime
        ]);

        $this->container->setDefinition('jsp.api_authentication.auth_token.payload_factory', $definition)
            ->setPublic(true);
        $this->container->setAlias(PayloadFactory::class,'jsp.api_authentication.auth_token.payload_factory')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerTokenFactory(): void
    {
        $definition = new Definition(TokenFactory::class, [
            new Reference('jsp.api_authentication.auth_token.header_factory'),
        ]);

        $this->container->setDefinition('jsp.api_authentication.auth_token.token_factory', $definition)
            ->setPublic(true);
        $this->container->setAlias(TokenFactory::class,'jsp.api_authentication.auth_token.token_factory')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerAuthTokenProvider(): void
    {
        $definition = new Definition(AuthTokenProvider::class, [
            new Reference('jsp.api_authentication.auth_token.header_factory'),
            new Reference('jsp.api_authentication.auth_token.payload_factory'),
            new Reference('jsp.api_authentication.auth_token.token_factory'),
        ]);

        $this->container->setDefinition('jsp.api_authentication.auth_token.provider', $definition)
            ->setPublic(true);
        $this->container->setAlias(AuthTokenProvider::class,'jsp.api_authentication.auth_token.provider')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerAuthTokenManager(): void
    {
        $definition = new Definition(AuthTokenManager::class, [
            new Reference('jsp.user.user_manager'),
            new Reference('jsp.api_authentication.auth_token.provider'),
        ]);

        $this->container->setDefinition('jsp.api_authentication.auth_token.manager', $definition)
            ->setPublic(true);
        $this->container->setAlias(AuthTokenManager::class,'jsp.api_authentication.auth_token.manager')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerTokenAuthenticator(): void
    {
        $definition = new Definition(TokenAuthenticator::class, [
            new Reference('doctrine.orm.entity_manager'),
            new Reference('jsp.api_authentication.auth_token.manager'),
            new Reference('security.user_password_encoder.generic'),
            $this->config[CFG::KEY_TOKEN_TO_USER],
        ]);

        $this->container->setDefinition('jsp.api_authentication.token_authenticator', $definition)
            ->setPublic(true);
        $this->container->setAlias(TokenAuthenticator::class,'jsp.api_authentication.token_authenticator')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerTokenInformationDtoBuilder(): void
    {
        $definition = new Definition(TokenInformationDtoBuilder::class, [
            new Reference('jsp.user.user_manager'),
            new Reference('jsp.api_authentication.auth_token.manager'),
            new Reference('jsp.api_authentication.auth_token.provider'),
        ]);

        $this->container->setDefinition('jsp.api_authentication.token_information_dto_builder', $definition)
            ->setPublic(true);
        $this->container->setAlias(TokenInformationDtoBuilderInterface::class,'jsp.api_authentication.token_information_dto_builder')
            ->setPublic(true);
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerApiV1AuthController(): void
    {
        $definition = new Definition(AuthController::class);
        $this->container->setDefinition('jsp.api_authentication.api_controller_v1.default_controller', $definition)
            ->addTag('controller.service_arguments')
            ->setPublic(true);
        $this->container->setAlias(AuthController::class, 'jsp.api_authentication.api_controller_v1.default_controller')
            ->setPublic(true);
    }
}
