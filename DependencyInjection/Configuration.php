<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\DependencyInjection;

use RuntimeException;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public const KEY_JSP_API_AUTH = 'jsp_api_authentication';

    public const KEY_TOKEN_TO_USER = 'token_to_user';
    public const KEY_TTU_TOKEN     = 'token';
    public const KEY_TTU_USER      = 'user';

    public const KEY_JWT                 = 'jwt';
    public const KEY_JWT_EXPIRATION      = 'expiration';
    public const KEY_JWT_EXPIRATION_TIME = 'time';
    public const KEY_JWT_ALGORITHM       = 'algorithm';
    public const KEY_JWT_ENCODING        = 'encoding';

    public const JWT_DEFAULT_EXPIRATION_TIME = 3600; /** 1 Hour in Seconds */
    public const JWT_DEFAULT_ALGORITHM       = 'sha256';

    /**
     * @throws RuntimeException
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::KEY_JSP_API_AUTH);

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->append($this->addXAuthTokenToUser())
            ->append($this->addJwtConfig())
        ;

        return $treeBuilder;
    }

    /**
     * @throws \RuntimeException
     */
    private function addXAuthTokenToUser(): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder(self::KEY_TOKEN_TO_USER);

        /** @var ArrayNodeDefinition $xAuthTokenNode */
        $xAuthTokenNode = $treeBuilder->getRootNode();

        $xAuthTokenNode
            ->canBeUnset()
            ->  useAttributeAsKey(self::KEY_TTU_TOKEN, false)
            ->  arrayPrototype()
            ->    addDefaultsIfNotSet()
            ->    children()
            ->      scalarNode(self::KEY_TTU_TOKEN)->isRequired()->end()
            ->      scalarNode(self::KEY_TTU_USER)->isRequired()->end()
            ->    end()
            ->  end()
            ->end()
        ;

        return $xAuthTokenNode;
    }

    /**
     * @throws \RuntimeException
     */
    private function addJwtConfig(): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder(self::KEY_JWT);

        /** @var ArrayNodeDefinition $jwtNode */
        $jwtNode = $treeBuilder->getRootNode();

        $jwtNode
            ->canBeUnset()
            ->  addDefaultsIfNotSet()
            ->  children()
            ->    append($this->addJwtExpiration())
            ->    append($this->addJwtAlgorithm())
            ->  end()
            ->end()
        ;

        return $jwtNode;
    }

    /**
     * @throws \RuntimeException
     */
    private function addJwtExpiration(): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder(self::KEY_JWT_EXPIRATION);

        /** @var ArrayNodeDefinition $jwtExpirationNode */
        $jwtExpirationNode = $treeBuilder->getRootNode();

        $jwtExpirationNode
            ->canBeUnset()
            ->  addDefaultsIfNotSet()
            ->  children()
            ->    integerNode(self::KEY_JWT_EXPIRATION_TIME)->defaultValue(self::JWT_DEFAULT_EXPIRATION_TIME)->end()
            ->  end()
            ->end()
        ;

        return $jwtExpirationNode;
    }

    /**
     * @throws \RuntimeException
     */
    private function addJwtAlgorithm(): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder(self::KEY_JWT_ENCODING);

        /** @var ArrayNodeDefinition $jwtAlgorithmNode */
        $jwtAlgorithmNode = $treeBuilder->getRootNode();

        $jwtAlgorithmNode
            ->canBeUnset()
            ->  addDefaultsIfNotSet()
            ->  children()
            ->    scalarNode(self::KEY_JWT_ALGORITHM)->defaultValue(self::JWT_DEFAULT_ALGORITHM)->end()
            ->  end()
            ->end()
        ;

        return $jwtAlgorithmNode;
    }
}
