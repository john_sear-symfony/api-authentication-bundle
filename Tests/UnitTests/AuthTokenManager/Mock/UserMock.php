<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenManager\Mock;

use JohnSear\JspUserBundle\Entity\User;

class UserMock
{
    public const USER_UUID = '1478c383-c2b1-c3b2-efbf-bd4cc387efbf';
    public const USER_LOGIN = 'john_doe';

    public function getUser(): User
    {
        return (new User(self::USER_UUID))
            ->setLogin(self::USER_LOGIN)
            ->setSalt('b2nOIBVAk0C3M8ZNRePuToSclwimtWDv')
            ;
    }
}
