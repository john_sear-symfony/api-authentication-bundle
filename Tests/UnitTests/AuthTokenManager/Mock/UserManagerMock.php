<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenManager\Mock;

use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UserManagerMock
{
    public function getUserManager(TestCase $testCase): MockObject
    {
        $mock = $testCase->getMockBuilder(UserManager::class)->disableOriginalConstructor()->getMock();
        $mock
            ->method('getUserByLogin')
            ->willReturnCallback(static function () {
                $args = func_get_args();

                $parameter = reset($args);

                if ($parameter !== UserMock::USER_LOGIN) {
                    return null;
                }

                return (new UserMock())->getUser();
            });

        return $mock;
    }
}
