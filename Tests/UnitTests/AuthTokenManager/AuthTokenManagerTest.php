<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenManager;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenManager;
use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenProvider;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\HeaderFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\PayloadFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\TokenFactory;
use JohnSear\JspApiAuthenticationBundle\Exception\AuthTokenInvalidException;
use JohnSear\JspApiAuthenticationBundle\Exception\InvalidTokenCreationArgumentException;
use JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenManager\Mock\UserManagerMock;
use JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenManager\Mock\UserMock;
use JohnSear\JspUserBundle\DependencyInjection\UserManager\UserManager;
use PHPUnit\Framework\TestCase;

class AuthTokenManagerTest extends TestCase
{
    private $sut;

    public function setUp()
    {
        $headerFactory = new HeaderFactory();
        $payloadFactory = new PayloadFactory();
        $tokenFactory = new TokenFactory($headerFactory);

        /** @var UserManager $userManagerMock */
        $userManagerMock = (new UserManagerMock())->getUserManager($this);
        $authTokenProvider = new AuthTokenProvider($headerFactory, $payloadFactory, $tokenFactory);

        $this->sut = new AuthTokenManager($userManagerMock, $authTokenProvider);
    }

    /**
     * @throws InvalidTokenCreationArgumentException
     * @throws AuthTokenInvalidException
     */
    public function test_encode_throwsOnNotFoundUserByLogin(): void
    {
        $this->expectException(InvalidTokenCreationArgumentException::class);

        $this->sut->encode('not_findable');
    }

    public function test_encode_withValidArguments(): void
    {
        $userLogin = UserMock::USER_LOGIN;

        /**
         * @noinspection PhpUnhandledExceptionInspection
         *
         * will not thrown because of valid arguments
         */
        $token = $this->sut->encode($userLogin);

        $tokenValue = $token->get();

        $expected = true;
        $actual = is_string($tokenValue);

        $this->assertEquals($expected, $actual);
    }
}
