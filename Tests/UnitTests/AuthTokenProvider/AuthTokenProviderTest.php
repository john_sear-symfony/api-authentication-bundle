<?php declare(strict_types=1);

namespace JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenProvider;

use JohnSear\JspApiAuthenticationBundle\AuthToken\AuthTokenProvider;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\HeaderFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\PayloadFactory;
use JohnSear\JspApiAuthenticationBundle\AuthToken\Factory\TokenFactory;
use JohnSear\JspApiAuthenticationBundle\Tests\UnitTests\AuthTokenProvider\Mock\UserMock;
use PHPUnit\Framework\TestCase;

class AuthTokenProviderTest extends TestCase
{
    private $sut;
    private $userMock;

    public function setUp()
    {
        $headerFactory = new HeaderFactory();
        $payloadFactory = new PayloadFactory();
        $tokenFactory = new TokenFactory($headerFactory);

        $this->sut = new AuthTokenProvider($headerFactory, $payloadFactory, $tokenFactory);

        $this->userMock = (new UserMock())->getUser();
    }

    public function test_encode_withValidArguments(): void
    {
        $header  = (new HeaderFactory())->create();
        $payload = (new PayloadFactory())->create($this->userMock);

        /**
         * @noinspection PhpUnhandledExceptionInspection
         *
         * will not thrown because of valid arguments
         */
        $token = $this->sut->encode($this->userMock, $header, $payload);

        $tokenBValue = $token->get();

        $expected = true;
        $actual = is_string($tokenBValue);

        $this->assertEquals($expected, $actual);
    }
}
